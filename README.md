# README #

1/3スケールのNEC PC-6601風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1983年11月21日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-6600%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA)
- [ボクたちが愛した、想い出のレトロパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1111538.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6601/raw/accfa7c1dffc9fecb1d9c5be8ecd14868525ff7b/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6601/raw/accfa7c1dffc9fecb1d9c5be8ecd14868525ff7b/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6601/raw/accfa7c1dffc9fecb1d9c5be8ecd14868525ff7b/ExampleImage.png)
